FROM debian

RUN apt-get update && apt-get install -y apache2 && apt-get clean
LABEL version=1.0
ENTRYPOINT [ "apachectl" ]
CMD [ "-D","foreground" ]
